/*
	eslint-disable
	require-jsdoc,
	no-sync,
	global-require,
*/
'use strict';

var mocha = require('mocha');
var path = require('path');
var fs = require('fs');
var config = require('./panic.config.js');
var panic = require('panic-server');
var spawn = require('child_process').fork;
var clients = panic.clients;

function exit () {
	process.exit();
}

mocha.describe('Server to server sync', function () {

	this.slow(2000);

	var server, processes, clientsArray;

	mocha.before(function (done) {

		// Start the panic server.
		server = panic.server();
		server.listen(config.port);
		var url = 'http://localhost:' + config.port;

		var clientJS = path.join(__dirname, 'client.js');

		// Spin up a number of processes.
		processes = Array(config.peers).fill().map(function () {
			return spawn(clientJS, [url]);
		});

		// A list of clients.
		clientsArray = [];

		var client, exclusion;

		// For each forked process...
		for (var index = 0; index < processes.length; index += 1) {

			// Create an exclusion group of every client already chosen.
			exclusion = new panic.ClientList(clientsArray);

			// Choose one client from the remaining group.
			client = panic.clients.excluding(exclusion).pluck(1);

			// Add it to the list of clients.
			clientsArray.push(client);
		}

		// Wait for them to connect to panic.
		clients.on('add', function cb () {
			if (clients.length >= processes.length) {
				clients.removeListener('add', cb);
				done();
			}
		});

	});

	mocha.it('should sync data between servers', function () {

		// Create a list of peer URLs.
		var peers = clientsArray.map(function (client, index) {
			return 'http://localhost:' + (config.port + index + 1);
		});

		// Frowned on by mocha overlords.
		mocha.after(function () {

			// Delete all the json files once the
			// tests are finished.
			peers.forEach(function (url) {

				// Get the port, then postfix '.json'.
				fs.unlinkSync(url.split(':')[2] + '.json');
			});
		});

		var promises = clientsArray.map(function (client, index) {

			// Start each server on a unique URL.
			return client.run(function () {
				var Gun = require('gun');

				// Give require a root-relative address.
				var peer = require(this.data.file);

				// Start the server.
				peer.server.listen(this.data.port);

				// The full url it's listening on.
				var myAddr = 'http://localhost:' + this.data.port;

				// Use the address to create a unique json filename.
				var file = this.data.port + '.json';

				// Get the list of all peer urls who aren't me.
				var peers = this.data.peers.filter(function (url) {
					return url !== myAddr;
				});

				// Create a new gun instance.
				var gun = new Gun({
					peers: peers,

					file: file,
				});

				// Attach gun to the server.
				gun.wsp(peer.server);

				// Put gun where other clients can get to it.
				peer.gun = gun.get('servers');

				// Create an update.
				var update = {};
				update[myAddr] = 'online.';

				// This update should sync to all other servers.
				gun.put(update);
			}, {
				file: path.join(__dirname, 'server.js'),

				// Increment the port by process index,
				// and remember it starts counting at 0.
				port: config.port + index + 1,
				peers: peers,
			});
		});

		function check (done) {
			this.timeout(2000);

			// Get the peer object.
			var peer = require(this.data.file);

			peer.gun.on(function (data) {

				// Number of properties, excluding metadata "_".
				var properties = Object.keys(data).length - 1;
				if (properties >= this.data.expected) {
					done();
				}
			});
		}

		// After each peer reports success, this runs.
		// Mocha will use the reject/resolve value of this
		// promise to pass/fail this test.
		return Promise.all(promises).then(function () {

			// Check every server, make sure they have the sum data.
			return clients.run(check, {
				file: path.join(__dirname, 'server.js'),
				expected: processes.length,
			});
		});

	});

	mocha.after(function () {

		// Kill the children (processes).
		clients.run(exit);
	});

});
