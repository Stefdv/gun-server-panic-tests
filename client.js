'use strict';

var serverAddr = process.argv[2];

if (!serverAddr) {
	throw new Error('No panic server url given.');
}

var panic = require('panic-client');
panic.server(serverAddr);
