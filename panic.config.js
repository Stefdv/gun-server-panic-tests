/* eslint-disable no-process-env*/
'use strict';
var port = Number(process.env.PORT) || 3000;

module.exports = {
	port: port,
	peers: 2,
};
